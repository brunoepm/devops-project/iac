module "gke-vpc" {
  source             = "./modules/network"
  CLUSTER_NAME = var.cluster_name
  CLUSTER_ENV  = var.cluster_env
  GCP_PROJECT  = var.gcp_project
}
module "gke" {
  source             = "./modules/cluster"
  

  CLUSTER_LOCATION              = var.gcp_zone
  CLUSTER_NAME                  = var.cluster_name
  CLUSTER_ENV                   = var.cluster_env
  CLUSTER_VERSION               = var.cluster_version
  GCP_PROJECT                   = var.gcp_project
  MACHINE_TYPE                  = var.machine_type
  NODES_DISK_SIZE               = var.nodes_disk_size
  NODE_COUNT                    = var.node_count
  PREEMPTIBLE_NODES             = var.preemptible_nodes
  REGIONAL_CLUSTER              = var.regional_cluster
  cluster_secondary_range_name  = module.gke-vpc.cluster_secondary_range_name
  services_secondary_range_name = module.gke-vpc.services_secondary_range_name
  vpc_network_name              = module.gke-vpc.vpc_network_name
  vpc_subnetwork_name           = module.gke-vpc.vpc_subnetwork_name
  http_load_balancing_disabled  = var.http_load_balancing_disabled
}

module "gitlab" {
  source = "./modules/gitlab"
  CLUSTER_NAME                  = var.cluster_name
  KUBERNETES_CA_CERT        = module.gke.cluster_ca_certificate
  CLUSTER_ENDPOINT              = module.gke.cluster_endpoint
  KUBERNETES_TOKEN              = data.google_client_config.google_current.access_token
  
}

module "dns" {
  source = "./modules/dns"
  GCP_PROJECT                  = var.gcp_project
  DNS_ZONE        = module.gke.cluster_ca_certificate
  DNS_NAME              = module.gke.cluster_endpoint
  GKE_LOAD_BALANCER             = module.gke.load-balancer-ip
  
}


resource "google_project_service" "dns-api" {
  project = var.gcp_project
  service = "dns.googleapis.com"
}

resource "google_dns_managed_zone" "dev" {
  name     = "dev-zone"
  dns_name = "bmarques.awesomeapp.com."
  depends_on = [google_project_service.dns-api]
}

resource "google_dns_record_set" "frontend" {
  name = "frontend.${google_dns_managed_zone.dev.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.dev.name

  rrdatas = [module.gke.load-balancer-ip]
}
