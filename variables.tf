variable "gcp_project" {
  description = "GPC project the cluster will be deployed"
}

variable "service_account_key" {
  default = "./creds/serviceaccount.json"
  description = "GCP service-account JSON key"
}

variable "cluster_name" {
  description = "Cluster name"
}

variable "CLUSTER_NAME" {
  description = "Cluster name"
}

variable "cluster_env" {
  description = "Cluster env"
}

variable "cluster_version" {
  default     = "latest"
  description = "Set an specific gke version or leave blank to use the latest"
}

variable "gcp_region" {
  default     = "us-central1"
  description = "Cluster region"
}

variable "gcp_zone" {
  default     = "us-central1-a"
  description = "Cluster zone"
}

variable "regional_cluster" {
  default = "false"
  description = "(true/false) Whether to create a regional cluster or not"
}

variable "preemptible_nodes" {
  default     = "true"
  description = "(true/false) Wether to use preemptible nodes or not in this workspace"
}

variable "node_count" {
  default     = "3"
  description = "Number of nodes to deployed on each zone"
}

variable "machine_type" {
  default     = "n1-standard-2"
  description = "GCE machine type to be used by the nodes"
}

variable "nodes_disk_size" {
  default = "50"
  description = "Disk size for each node"
}

variable "http_load_balancing_disabled" {
  default = "false"
  description = <<EOF
The status of the HTTP (L7) load balancing controller addon, which makes it
easy to set up HTTP load balancers for services in a cluster. It is enabled
by default; set disabled = true to disable.
EOF
}
variable "gke_version" {
  default = "latest"
  description = "Which gke version to deploy"
}
variable "admin_user" {
  default = ""
  description = "admin user for kubernetes secretes"
}
variable "admin_password" {
  default = ""
  description = "admin password for kubernetes secrets"
}


variable "DNS_ZONE" {
  default = "myapp.com."
  description = "Name of the DNS Zone"
}

variable "DNS_NAME" {
  default = "dev-zone"
  description = "dns entry name"
}