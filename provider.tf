provider "google" {
  credentials = var.service_account_key
  project     = var.gcp_project
  region      = var.gcp_region
  version     = "3.9.0"
}

data "google_client_config" "google_current" {
  provider = "google"
}

provider "kubernetes" {
  load_config_file = false
  host = "https://${module.gke.cluster_endpoint}"
  cluster_ca_certificate = base64decode(module.gke.cluster_ca_certificate)
  token = data.google_client_config.google_current.access_token
}

provider "helm" {
  version = "1.1.1"
  kubernetes {
    load_config_file = false
    host     = "https://${module.gke.cluster_endpoint}"
    cluster_ca_certificate = base64decode(module.gke.cluster_ca_certificate)
    token = data.google_client_config.google_current.access_token
  }
}

provider "gitlab" {
}