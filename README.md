# Terraform + Gitlab CI Pipeline Example

This project intends to demonstrate the use of terraform + Gitlab CI for provisioning a kubernetes cluster in GKE, automatically  integrating it to the gitlab CI project and then triggering the deploy of a frontend + backend application.

## Setup:

1 - Gcloud CLI tool https://cloud.google.com/sdk/docs/quickstart-linux

2 - A GCP service account to be used by terraform  https://www.terraform.io/docs/providers/google/guides/getting_started.html#adding-credentials

3 - a GCP Bucket needed for storing the terraform state

4 - A gitlab personal access token with API scope access https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html, this token should be stored in GITLAB_TOKEN environment variable, this way terraform will be able to authenticate to your gitlab account to setup the integration with the Kubernetes cluster

## Running

With the prerequisites configured, just run

- **terraform init**
- **terraform plan -var-file** *.tfvarsfile* 
- **terraform apply -var-file** *.tfvarsfile*

The repository also has a gitlab CI pipeline configured, to avoid exposing the service account authentication file and the gitlab CI access token they have been stored inside Gitlab as protected variables

**SERVICEACCOUNT** has the contents of serviceaccount.json encoded in a base64 string
**GITLAB_TOKEN** gitlab access token

The pipeline will automatically execute the init, validate and plan terraform steps leaving apply and destroy to be applied manually as they involve potentially destructive changes to the environment

The pipeline has 2 plan stages that will be executed according to the repository branch, on master branch it will execute the plan using the tfvars file with the production variables, otherwise it will use another tfvars with variables for a development environment after executing the *terraform plan* the execution plan is saved as an artifact to be used in the following stages.

## Terraform components

A main file was created referencing 4 different modules, responsible for the main components of the environment 
- **Cluster** - resources required to create a GKE cluster, with attributes such as kubernetes version, region, node pool size, node type, and create a nginx ingress controller to be used as a reverse proxy for the environment
- **DNS** - registers the load balancer IP created during NGINX Ingress controller installation in GKE and creates a CloudDNS entry for it
- **Network** - VPC setup, subnet and firewall rules
- **Gitlab** - create a service account to be used by gitlab CI in the kubernetes cluster, and registers it as a cluster within the gitlab group making it accessible to all group projects

At the end of the execution the expected result is an operational gke cluster with a dns entry created for its ingress controller and the cluster registered on gitlab
## Application deploy

This repository's pipeline has a trigger configured to trigger another project within the same gitlab group, this project https://gitlab.com/brunoepm/devops-project/app-backend this project builds a backend application , builds a docker image, and applies a set of kubernetes manifests to deploy the app in the newly created cluster, this pipeline in turn triggers another pipeline that repeats the process to the frontend https://gitlab.com/brunoepm/devops-project/app-frontend


