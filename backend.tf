terraform {
    backend "gcs" {
      credentials = "creds/serviceaccount.json"
      bucket = "brunoepm-tf-test" 
      prefix = "terraform/state"
      }
  }
