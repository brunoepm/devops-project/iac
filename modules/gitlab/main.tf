data "google_client_config" "google_current" {
  provider = "google"
}

resource "kubernetes_service_account" "gitlab-admin" {
  metadata {
    name      = "gitlab-admin"
    namespace = "kube-system"
  }
}


data "gitlab_group" "my-group" {
  full_path = "brunoepm/devops-project"
}

resource "kubernetes_secret" "gitlab-admin" {
  metadata {
    name      = "gitlab-admin"
    namespace = "kube-system"
    annotations = {
      "kubernetes.io/service-account.name" = "${kubernetes_service_account.gitlab-admin.metadata.0.name}"
    }
  }
  lifecycle {
    ignore_changes = [
      data
    ]
  }
  type = "kubernetes.io/service-account-token"
}

resource "kubernetes_cluster_role_binding" "gitlab-admin" {
  metadata {
    name = "gitlab-admin"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "gitlab-admin"
    namespace = "kube-system"
  }
}



data "kubernetes_secret" "gitlab-admin-token" {
  metadata {
    name      = "${kubernetes_service_account.gitlab-admin.default_secret_name}"
    namespace = "kube-system"
  }
}


resource gitlab_group_cluster "gke" {
  group                       = "${data.gitlab_group.my-group.id}"
  name                          = "${var.CLUSTER_NAME}"
  enabled                       = true
  kubernetes_api_url            = "https://${var.CLUSTER_ENDPOINT}"
  kubernetes_token              = data.kubernetes_secret.gitlab-admin-token.data.token
  kubernetes_ca_cert            = "${trimspace(base64decode(var.KUBERNETES_CA_CERT))}"
  kubernetes_authorization_type = "rbac"
  environment_scope             = "*"
}