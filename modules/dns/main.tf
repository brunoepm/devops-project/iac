resource "google_project_service" "dns-api" {
  project = var.GCP_PROJECT
  service = "dns.googleapis.com"
}

resource "google_dns_managed_zone" "dev" {
  name     = var.DNS_ZONE
  dns_name = var.DNS_NAME
  depends_on = [google_project_service.dns-api]
}

resource "google_dns_record_set" "frontend" {
  name = "frontend.${var.DNS_NAME}"
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.dev.name

  rrdatas = [var.GKE_LOAD_BALANCER]
}
