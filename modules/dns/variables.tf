variable "GCP_PROJECT" {
  description = "GCP Project name"
}

variable "DNS_ZONE" {
  description = "Name of the DNS Zone"
}

variable "DNS_NAME" {
  default = "latest"
  description = "dns entry name"
}

variable "GKE_LOAD_BALANCER" {
  description = "GKE load balancer IP to be configured as a DNS A entry"
}