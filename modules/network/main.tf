resource "google_project_service" "cloudresourcemanager-api" {
  project = var.GCP_PROJECT
  service = "cloudresourcemanager.googleapis.com"
}
resource "google_project_service" "oslogin-api" {
  project    = var.GCP_PROJECT
  service    = "oslogin.googleapis.com"
  depends_on = [google_project_service.cloudresourcemanager-api]
}
resource "google_project_service" "compute-api" {
  project    = var.GCP_PROJECT
  service    = "compute.googleapis.com"
  depends_on = [google_project_service.oslogin-api]
}

resource "google_compute_network" "vpc" {
  name = "${var.CLUSTER_NAME}-${var.CLUSTER_ENV}-vpc"
  auto_create_subnetworks = "false"
  depends_on = [google_project_service.compute-api]
}

resource "google_compute_firewall" "default" {
  name    = "basic-firewall"
  network = google_compute_network.vpc.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "1000-2000"]
  }

  source_tags = ["web"]
}

# https://www.terraform.io/docs/providers/google/r/compute_subnetwork.html
resource "google_compute_subnetwork" "vpc_subnetwork" {

  name = "${google_compute_network.vpc.name}-subnetwork"



  ip_cidr_range = var.vpc_subnetwork_cidr_range

  network = google_compute_network.vpc.name

  secondary_ip_range = [
    {
      range_name    = var.cluster_secondary_range_name
      ip_cidr_range = var.cluster_secondary_range_cidr
    },
    {
      range_name    = var.services_secondary_range_name
      ip_cidr_range = var.services_secondary_range_cidr
    },
  ]


  private_ip_google_access = true
}
