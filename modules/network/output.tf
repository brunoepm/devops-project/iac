output "vpc_network_name" {
  value = google_compute_network.vpc.name
}

output "vpc_subnetwork_name" {
  value = google_compute_subnetwork.vpc_subnetwork.name
}

output "cluster_secondary_range_name" {
  value = var.cluster_secondary_range_name
}

output "services_secondary_range_name" {
  value = var.services_secondary_range_name
}
