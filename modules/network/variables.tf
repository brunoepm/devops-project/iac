variable "CLUSTER_NAME" {
  description = "Cluster name"
}
variable "CLUSTER_ENV" {
  description = "Cluster environment"
}

variable "GCP_PROJECT" {
  description = "GCP target project"
}
variable "vpc_subnetwork_cidr_range" {
  default = "10.0.16.0/20"
}
variable "cluster_secondary_range_name" {
  default = "pods"
  description = <<EOF
The name of the secondary range to be used as for the cluster CIDR block.
The secondary range will be used for pod IP addresses. This must be an
existing secondary range associated with the cluster subnetwork.
EOF
}
variable "cluster_secondary_range_cidr" {
  default = "10.16.0.0/12"
}
variable "services_secondary_range_name" {
  default = "services"
  description = <<EOF
The name of the secondary range to be used as for the services CIDR block.
The secondary range will be used for service ClusterIPs. This must be an
existing secondary range associated with the cluster subnetwork.
EOF
}
variable "services_secondary_range_cidr" {
  default = "10.1.0.0/20"
}
