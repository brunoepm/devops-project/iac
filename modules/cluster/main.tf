resource "google_project_service" "cloudresourcemanager-api" {
  project = var.GCP_PROJECT
  service = "cloudresourcemanager.googleapis.com"
}

resource "google_project_service" "gke-api" {
  project    = var.GCP_PROJECT
  service    = "container.googleapis.com"
  depends_on = [google_project_service.cloudresourcemanager-api]
}

resource "google_container_cluster" "gke" {
  provider           = google
  project            = var.GCP_PROJECT
  name               = "${var.CLUSTER_NAME}-${var.CLUSTER_ENV}"
  location           = "us-central1-a"
  min_master_version = var.CLUSTER_VERSION
  network            = var.vpc_network_name
  subnetwork         = var.vpc_subnetwork_name

  
  #remove default since we are creating our own node pool at the below resource
  remove_default_node_pool = true
  initial_node_count       = 1

  # Disable basic authentication
  master_auth {
    username = ""
    password = ""
    client_certificate_config {
      issue_client_certificate = false
    }
  }

  addons_config {

    http_load_balancing {
      # (true/false) Based on some examples they keep it enable by default so I did the same at vars and I will dive into this later
      disabled = var.http_load_balancing_disabled
    }
    # Enable the network policy addon for the master
    
  }

  # Activate IP aliases and use secondary ranges for pods and services
  ip_allocation_policy {
    cluster_secondary_range_name  = var.cluster_secondary_range_name
    services_secondary_range_name = var.services_secondary_range_name
  }

  
  timeouts {
    update = "30m"
    create = "30m"
    delete = "30m"
  }

  depends_on = [google_project_service.gke-api]
}

resource "google_container_node_pool" "gke_nodes" {
  project  = var.GCP_PROJECT
  provider = google
  name     = "nodes"
  location = "us-central1-a"
  cluster  = google_container_cluster.gke.name

  #if location is a region this terraform will provision one node in each zone thus making this setting deploy 3x
  node_count = var.NODE_COUNT

  management {
    auto_repair  = true
    auto_upgrade = true
  }
  
  autoscaling {
    min_node_count = var.MIN_NODE_COUNT
    max_node_count = var.MAX_NODE_COUNT
  }

  node_config {
    preemptible  = var.PREEMPTIBLE_NODES
    machine_type = var.MACHINE_TYPE
    disk_size_gb = var.NODES_DISK_SIZE
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }

  timeouts {
    create = "30m"
    update = "20m"
    delete = "20m"
  }
}
data "helm_repository" "stable" {
  name = "stable"
  url  = "https://kubernetes-charts.storage.googleapis.com"
}

resource "helm_release" "nginx_ingress" {
  depends_on = [google_container_node_pool.gke_nodes]
  repository = data.helm_repository.stable.url
  chart         = "nginx-ingress"
  name          = "nginx-ingress"
  namespace     = "kube-system"
  values = [<<EOF
controller:
  config:
    use-forwarded-headers: "true"
  publishService:
    enabled: "true"
  resources:
    limits:
      cpu: 200m
      memory: 128Mi
    requests:
      cpu: 100m
      memory: 64Mi
  service:
    annotations:
      service.beta.kubernetes.io/external-traffic: OnlyLocal
    externalTrafficPolicy: "Local"
defaultBackend:
  service:
    annotations:
      nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
      nginx.ingress.kubernetes.io/from-to-www-redirect: "true"
EOF
  ]
}

data "kubernetes_service" "nginx_ingress_controller" {
  depends_on = [helm_release.nginx_ingress]

  metadata {
    name      = "${helm_release.nginx_ingress.metadata[0].name}-controller"
    namespace = helm_release.nginx_ingress.metadata[0].namespace
  }
}

