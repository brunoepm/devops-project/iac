variable "GCP_PROJECT" {
  description = "GCP Project where the culster will be deployed"
}


variable "CLUSTER_NAME" {
  description = "Cluster name"
}

variable "CLUSTER_ENV" {
  description = "Cluster environment"
}

variable "CLUSTER_VERSION" {
  default = "latest"
  description = "Set an specific gke version or leave blank to use the latest"
}

variable "CLUSTER_LOCATION" {
  description = "Cluster location - Always use a zone here, if you want a regional cluster set the variable REGIONAL_CLUSTER to true"
}

variable "REGIONAL_CLUSTER" {
  default = "false"
  description = "Wether to deploy the cluster as a regional cluster"
}

variable "PREEMPTIBLE_NODES" {
  default     = "true"
  description = "(true/false) Wether to use preemptible nodes or not in this workspace"
}

variable "NODE_COUNT" {
  default = "3"
  description = "Number of nodes to deployed on each zone"
}

variable "MIN_NODE_COUNT" {
  default = "3"
  description = "Minimum number of nodes to be keep by autoscaling"
}

variable "MAX_NODE_COUNT" {
  default = "3"
  description = "Maximum num"
}

variable "MACHINE_TYPE" {
  default = "n1-standard-1"
  description = "GCE machine type to be used by the nodes"
}

variable "NODES_DISK_SIZE" {
  default = "50"
  description = "Disk size for each node"
}

variable "vpc_network_name" {
  description = <<EOF
VPC Network to be used by the cluster
EOF
}

variable "vpc_subnetwork_name" {
  description = <<EOF
VPC Subnetwork to be used by the cluster
EOF
}

variable "cluster_secondary_range_name" {
  description = <<EOF
The name of the secondary range to be used as for the cluster CIDR block.
The secondary range will be used for pod IP addresses. This must be an
existing secondary range associated with the cluster subnetwork.
EOF
}

variable "services_secondary_range_name" {
  description = <<EOF
The name of the secondary range to be used as for the services CIDR block.
The secondary range will be used for service ClusterIPs. This must be an
existing secondary range associated with the cluster subnetwork.
EOF
}

variable "http_load_balancing_disabled" {
  default = "false"
  description = <<EOF
The status of the HTTP (L7) load balancing controller addon, which makes it
easy to set up HTTP load balancers for services in a cluster. It is enabled
by default; set disabled = true to disable.
EOF
}
